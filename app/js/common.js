var swiper = new Swiper('.swiper-container', {
    slidesPerView: 'auto',
    spaceBetween: 1
});

const checkSwipper = () => {
    if ($(window).width() > 992) {
        swiper.destroy();

    }
};

$(window).on('resize', function () {
    checkSwipper();
    // appendCatalog();
    openMnu();
    // console.log('fsd')
});

$(document).ready(function () {
    checkSwipper();
    appendCatalog();
    toggleMain();
    openMnu();



    jQuery('.close-tab').on('click', function (e) {
        let $this = $(this),
            modal = $this.find('.modal-ty__wrap');

        $this.parent().parent().toggleClass('open-modal');
    });

    $('.contact-us__phone').mask("+38099  999  99  99");



    let swiperMain = new Swiper('.swiper-main', {
        slidesPerView: 'auto',
        slidesPerColumn: 1,
        // centeredSlides: true,
        spaceBetween: 10,
        navigation: {
            nextEl: '.swiper-button-next1',
            prevEl: '.swiper-button-prev1',
        }

    });
    let swiperAside = new Swiper('.swiper-container__aside', {
        slidesPerView: 1,
        slidesPerColumn: 4,
        spaceBetween: 20,

        breakpoints: {
            992: {
                slidesPerView: 2,
                slidesPerColumn: 2,
            },
            620: {
                slidesPerView: 1,
                slidesPerColumn: 4,
                spaceBetween: 0
            }
        },
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev',
        }

    });


    let swiperFilter = new Swiper('.swiper-container__filter', {
        slidesPerView: 1,
        slidesPerColumn: 1,
        spaceBetween: 30,
        init: false,
        breakpoints: {
            992: {
                spaceBetween: 30
            },
            620: {
                spaceBetween: 30
            }
        },
        navigation: {
            nextEl: '.swiper-button-next6',
            prevEl: '.swiper-button-prev6',
        },
        on: {
            init: function () {
                console.log('swiper initialized');
            },
        },

    });
    let swiperFilters = new Swiper('.swiper-container__filters', {
        slidesPerView: 1,
        slidesPerColumn: 1,
        spaceBetween: 30,
        breakpoints: {
            992: {
                spaceBetween: 30
            },
            620: {
                spaceBetween: 30
            }
        },
        navigation: {
            nextEl: '.swiper-button-next7',
            prevEl: '.swiper-button-prev7',
        },
        // on: {
        //     init: function () {
        //         console.log('swiper initialized');
        //     },
        // },

    });


    let swiperAside3 = new Swiper('.swiper-container__aside3', {
        slidesPerView: 1,
        slidesPerColumn: 4,
        spaceBetween: 20,
        breakpoints: {
            992: {
                slidesPerView: 2,
                slidesPerColumn: 2,
            },
            620: {
                slidesPerView: 1,
                slidesPerColumn: 4,
                spaceBetween: 0
            }
        },
        navigation: {
            nextEl: '.swiper-button-next3',
            prevEl: '.swiper-button-prev3',
        }

    });

    let swiperAside4 = new Swiper('.swiper-container__aside4', {
        slidesPerView: 2,
        slidesPerColumn: 2,
        spaceBetween: 20,
        breakpoints: {
            992: {
                slidesPerView: 2,
                slidesPerColumn: 2,
            },
            620: {
                slidesPerView: 1,
                slidesPerColumn: 4,
                spaceBetween: 0
            }
        },
        navigation: {
            nextEl: '.swiper-button-next4',
            prevEl: '.swiper-button-prev4',
        }

    });
    let swiperAside2 = new Swiper('.swiper-container__aside2', {
        slidesPerView: 1,
        slidesPerColumn: 2,
        spaceBetween: 30,
        navigation: {
            nextEl: '.swiper-button-next2',
            prevEl: '.swiper-button-prev2',
        },
        breakpoints: {
            992: {
                slidesPerView: 2,
                slidesPerColumn: 1,
            },
            620: {
                slidesPerView: 1,
                slidesPerColumn: 2,
                spaceBetween: 0
            }
        },
    });

    (function ($) {
        $(function () {

            $('ul.tabs__caption').on('click', 'li:not(.active)', function (e) {
                e.preventDefault();
                $(this)
                    .addClass('active').siblings().removeClass('active')
                    .closest('div.tabs').find('div.tabs__content').removeClass('active').eq($(this).index()).addClass('active');
            });
        });
    })(jQuery);


    jQuery('.tabs-head__toggle').on('click', function (e) {
        let $this = $(this),
            icon = $this.find('.fas');

        $this.parent().find('.tabs__content__flex').toggle('show');
        icon.toggleClass('fa-caret-down fa-caret-up');
    });
    jQuery('.tab-mobite__head').on('click', function (e) {
        let $this = $(this),
            icon = $this.find('.fas');

        $this.parent().find('.tabs__content').toggle('show');
        $this.toggleClass('inactive active');
    });
    let $this = $(this);
    jQuery('.tab-head').on('click', function (e) {
        let $this = $(this),
            icon = $this.find('.close-tab');

        $this.parent().find('.tab-body').toggle('show');
        icon.toggle('show');
        $this.closest('.tab-content').toggleClass('bg');
        $this.toggleClass('green');
    });

    jQuery('.filter-js').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        let icon = $this.parents().find('.filter-js .fas');
        console.log(icon);
        swiperFilter.init();
        $('.modal-filters-all').toggleClass('opacity-modal');
        icon.toggleClass('fa-filter fa-times');


    });
    jQuery('.head-modal__tab').on('click', function (e) {
        e.preventDefault();
        let $this = $(this),
            icon = $this.parent().find('.fas');
            tabsBody = $this.parent().find('.body-modal__tab');
        
        tabsBody.toggle('show');
        $this.toggleClass('border-n');
        icon.toggleClass('fa-sort-down fa-times');
    });


    jQuery('.toggle-tabs').on('click', function (e) {
        let $this = $(this),
            icon = $this.parent().find('.fas');

        $this.parent().find('.col-tab').toggle('show');
        icon.toggleClass('fa-sort-down fa-sort-up');
    });



    jQuery('.nav-search').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        $('.top-search-js').toggleClass('open-nav');
        $('.wrap-top__search').toggleClass('open-wrap');
        $this.toggleClass('active');

    });

    jQuery('.catalog-company').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        $('.top-catalog-js').toggleClass('open-nav');
        $this.toggleClass('active');
        $(document).mouseup(function (e) {
          var container = $(".top-catalog-js");
          if (container.has(e.target).length === 0){
            container.removeClass('open-nav');
          }
        }); 
    });
    jQuery('.sort-js').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);
        let icon = $this.parents().find('.icon-sort');
        let icon2 = $this.parents().find('.sort-js .fa-times');
        $('.modal-filters').toggleClass('open');
        icon.toggleClass('svg-dn');
        icon2.toggleClass('icn-db');
    });


    $('.col-tab li').on('click', function (e) {
        e.preventDefault();
        let $this = $(this);

        if ($this.hasClass('active') === false) {
            let trigger = $this.data('trigger');

            $('.tab-wrap').addClass('hidden');
            $(trigger).removeClass('hidden');

            $('.col-tab li').removeClass('active');
            $this.addClass('active');
        }


    });
    jQuery('.menu-fast__320').on('click', function (e) {
        e.preventDefault();
        let $this = $(this),
            icon = $this.find('.icon-d');

        $('.menu-fast').toggleClass('open');
        icon.toggleClass('fa-sort-down fa-sort-up');
    });

    $("input:checkbox:not('.multi-check')").on('click', function () {
        var $box = $(this);
        if ($box.is(":checked")) {
            var group = "input:checkbox[name='" + $box.attr("name") + "']";
            $(group).prop("checked", false);
            $box.prop("checked", true);
        } else {
            $box.prop("checked", false);
        }
    });

    $('.add-number-js').on('click', function (e) {
        e.preventDefault();
        let htmlAddNum = `<div class="wrap-for-add-num added-num"><div class="close-tab"></div><input class="add-company__input input" type="tel" required="" placeholder="+380 _ _  _ _ _  _ _  _ _" value="+380" maxlength="13" minlength="5"></div>`;
        $('.wrap-option__tar').append(htmlAddNum);
    });

    $('.wrap-option.wrap-option__tar').on('click', '.close-tab', function () {
        let $this = $(this);
        $this.parent('.added-num').remove();
    });

    jQuery('.item-map').on('click', function (e) {
        e.preventDefault
        let $this = $(this);
        $this.parent().find('.content').toggle('show');
    });
    jQuery('.item-map-in').on('click', function (e) {
        e.preventDefault
        let $this = $(this);
        $this.parent().find('.content-in').toggle('show');
    });

    jQuery('.item-card-header .btn').on('click', function (e) {
        e.preventDefault
        let thisEl = $(this);
        thisEl.parent().next().toggleClass('show');
    });
    jQuery('.item-map-in').on('click', function (e) {
        e.preventDefault
        let $this = $(this);
        $this.parent().find('.content-in').toggle('show');
    });

});

let toggleMain = () => {
    if ($(window).width() < 992) {
        jQuery('.main-row__toggle').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            let icon = $this.parent().find('.fas');
            $this.parent().find('.main-body__toggle').toggle('show');
            icon.toggleClass('fa-sort-down fa-times');
        });
    }
};
let openMnu = () => {
    if ($(window).width() > 992) {
        jQuery('.nav-mnu, .top-nav').hover(function () {
            let $this = $(this);
            $('.top-nav-js').toggleClass('open-nav');
            $this.toggleClass('active');
            console.log('h')
        });

    } else {
        jQuery('.nav-mnu').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            $('.top-nav-js').toggleClass('open-nav');
            console.log('c')
        });
    }
};


        jQuery('.detect-city').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            $('.top-location-js-mob').toggleClass('open-nav');

        });
        jQuery('.another-js').on('click', function (e) {
            e.preventDefault();
            let $this = $(this);
            $('.top-location-js');
            $this.parents().find('.top-location-js').toggleClass('open-nav');
            $('.top-location-js-mob').toggleClass('open-nav');
            $(document).mouseup(function (e) {
              var container = $(".top-location-js");
              if (container.has(e.target).length === 0){
                container.removeClass('open-nav');
              }
            });
            $(document).mouseup(function (e) {
              var container = $(".top-location-js-mob");
              if (container.has(e.target).length === 0){
                container.removeClass('open-nav');
              }
            });

        });


$('.submit1').on('click', function(e){
  e.preventDefault();  
  var checkboxes = $('input[type="checkbox"]:checked');
  for (let i = 0; i < checkboxes.length; i++) {
      let valueEl = checkboxes.eq(i).prop("value");
      let tagBox = $('.tag-box');
    tagBox.append('<div class="tag">'+ valueEl +'<a class="remove" href="#"><i class="fas fa-times"></i></a></div>');
  }
  let $this = $(this);
  let icon = $this.parents().find('.filter-js .fas');
  icon.toggleClass('fa-filter fa-times');
  $('.modal-filters-all').removeClass('opacity-modal');
  removeTag ();
});

$('.close-windows').on('click', function () {
   let $this = $(this);
   $('.top-catalog-js').removeClass('open-nav');
   $('.top-location-js').removeClass('open-nav');

});

$('.submit').on('click', function(e){
    e.preventDefault();
    var checkboxes = $('input[type="checkbox"]:checked');
    for (let i = 0; i < checkboxes.length; i++) {
        let valueEl = checkboxes.eq(i).prop("value");
        let tagBox = $('.tag-box');
        tagBox.append('<div class="tag">'+ valueEl +'<a class="remove" href="#"><i class="fas fa-times"></i></a></div>');
    }
    let $this = $(this);
    let icon = $this.parents().find('.icon-sort');
    let icon2 = $this.parents().find('.sort-js .fa-times');
    $('.modal-filters').removeClass('open');
    $('.modal-filters-all').removeClass('opacity-modal');
    icon.removeClass('svg-dn');
    icon2.removeClass('icn-db');
    $('.modal-filters').removeClass('open');
});

$('.tag-box ').on('click', '.tag', function (e) {
    e.preventDefault();
    $(this).remove('.tag');
});


$('.add-tag__js').on('click', function(e){
    e.preventDefault();
    let $this = $(this);
    $('.modal-filters-all').toggleClass('opacity-modal');
    $(document).mouseup(function (e) {
        var container = $(".modal-filters-all");
        if (container.has(e.target).length === 0){
          container.removeClass('opacity-modal');
        }
      });
});

let appendCatalog = () => {
    let firstBlock = $('.ritual-catalog-js'),
        secondBlock = $('.second-block-js'),
        bannerMove = $('.banner-move-js')
    ;

    if ($(window).width() < 992) {
        bannerMove.appendTo('.banner-resize-append');
        firstBlock.appendTo('.first-bar-small');
        secondBlock.appendTo('.second-bar-small');
    } else {
        bannerMove.appendTo('.banner-move');
        firstBlock.appendTo('.first-bar-hight');
        secondBlock.appendTo('.second-bar-hight');

    }


};

// function getFocus(campo){
//     $(window).bind('resize', function() {
//         if ($(window).width() <= 992) {
//             $(campo).focus();
//         }
//     });
//
// }
// $('input').click(function(){
//     if (input.has(e.target).length === 0) {
//         input.blur();
//     } else {
//
//     }
//     getFocus(this);
//     console.log(this);
//
// });

// $(window).click(function (e) {
//     let input = $('input');
//     if (input.has(e.target).length === 0){
//         input.blur();
//     } else {
//         $(campo).focus();
//         getFocus(this);
//     }
// });
